var express = require('express');
var app = express();
var request = require('request');
var SpotifyWebApi = require('spotify-web-api-node');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var createError = require('http-errors')




var spotifyApi = new SpotifyWebApi({
    clientId: 'e6d46a9af02d4ad2b5dce270678a0451',
    clientSecret: 'd0b94f7c44bf48609da31afa32406763'
  });
  var albums = new Map()
  function getTrackInfo(id, cb){
      if(id == null) return;
    request({
        headers: {
            'Authorization':`Bearer ${spotifyApi.getAccessToken()}`
        },
        uri: "https://api.spotify.com/v1/albums/" + id,
        method: 'GET'
    }, function (err, res2, body) {
        if(body == undefined) return;
        const data = JSON.parse(body)
        cb(data)
    });
  }





  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'hbs');
  
  //app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));

/* GET home page. */
app.get('/musik', function(req, res, next) {

    spotifyApi.clientCredentialsGrant().then(
        function(authdata) {
          console.log('The access token is ' + authdata.body['access_token']);
          spotifyApi.setAccessToken(authdata.body['access_token']);
            request({
                headers: {
                    'Authorization':`Bearer ${authdata.body['access_token']}`
                },
                uri: 'https://api.spotify.com/v1/playlists/4fmkANXA1ZGQcBFWn7LOkW',
                method: 'GET'
            }, function (err, res2, body) {
                const data = JSON.parse(body)
                var current_index = 0
                data.tracks.items.forEach(function(track, index, array){
                    if(track == null) return;
                    const data = track.track.album
                    albums.set(data.name, {
                        name: data.name,
                        image: data.images[0],
                        url: data.external_urls.spotify,
                        released:  data.release_date,
                        artist: {
                            name: data.artists[0]
                        },
                    })
                })
                res.render('home', {layout: 'layout', albums: Array.from(albums.values())})
            });

        },
        function(err) {
          console.log('Something went wrong!', err);
        }
      );

    spotifyApi.getAccessToken()
});

app.get('/musik/song/:id', function(req, res, next) {

    spotifyApi.clientCredentialsGrant().then(
        function(authdata) {
          console.log('The access token is ' + authdata.body['access_token']);
          spotifyApi.setAccessToken(authdata.body['access_token']);
            request({
                headers: {
                    'Authorization':`Bearer ${authdata.body['access_token']}`
                },
                uri: 'https://api.spotify.com/v1/tracks/' + req.params.id,
                method: 'GET'
            }, function (err, res2, body) {
                const data = JSON.parse(body)
                console.log(data)
                res.render('song', {layout: 'layout', song: {
                    name: data.name,
                    released: data.album.release_date,
                    cover: data.album.images[0].url,
                    artist: data.artists[0].name,
                    url: data.external_urls.spotify
                }})
            });

        },
        function(err) {
          console.log('Something went wrong!', err);
        }
      );

    spotifyApi.getAccessToken()
});

app.get('/musik/playlist/:id', function(req, res, next) {

  spotifyApi.clientCredentialsGrant().then(
      function(authdata) {
        console.log('The access token is ' + authdata.body['access_token']);
        spotifyApi.setAccessToken(authdata.body['access_token']);
          request({
              headers: {
                  'Authorization':`Bearer ${authdata.body['access_token']}`
              },
              uri: 'https://api.spotify.com/v1/playlists/' + req.params.id,
              method: 'GET'
          }, function (err, res2, body) {
              const data = JSON.parse(body)
              var current_index = 0
              data.tracks.items.forEach(function(track, index, array){
                  if(track == null) return;
                  const data = track.track.album
                  albums.set(data.name, {
                      name: data.name,
                      image: data.images[0],
                      url: data.external_urls.spotify,
                      released:  data.release_date,
                      artist: {
                          name: data.artists[0]
                      },
                  })
              })
              res.render('playlist', {layout: 'layout', albums: Array.from(albums.values()), playlist_name: data.name})
          });

      },
      function(err) {
        console.log('Something went wrong!', err);
      }
    );

  spotifyApi.getAccessToken()
});




// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});
  
  // error handler
app.use(function(err, req, res, next) {
    
    //console.log(err)
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    const images = ["https://media2.giphy.com/media/9J7tdYltWyXIY/giphy.gif", "/images/dipper.png"]
    var item = images[Math.floor(Math.random()*images.length)];
  
    if(err.status == 404) return res.render('404', {image: item})
  
    // render the error page
    res.status(err.status || 500);
    res.render('general-error');
  });




app.listen(4000, function(){
    console.log(`Started Musik on port 8000\n    - Created By Owen Rummage`)
})